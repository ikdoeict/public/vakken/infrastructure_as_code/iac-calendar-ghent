# Readme

## running pytest

to run pytest in Django, make sure your working directory is set to the events directory. 
Also, make sure `pytest-django` has been installed.


```bash
[student@machine6 events]$ pytest
===================================================== test session starts =====================================================
platform linux -- Python 3.9.18, pytest-8.2.2, pluggy-1.5.0
django: version: 4.2.13, settings: events.settings-test (from ini)
rootdir: /home/student/iac-calendar-ghent/project/containers/django-rest/source/events
configfile: pytest.ini
plugins: django-4.8.0
collected 2 items                                                                                                             

events/tests/tests.py ..                                                                                                [100%]

====================================================== 2 passed in 0.99s ======================================================
[student@iac-Mgmt-Ansible-host-roel-vansteenberghe-5006 events]$ 
```