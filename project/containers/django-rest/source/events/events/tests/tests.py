import pytest
from events.models import Event
from django.contrib.auth.models import User

@pytest.mark.django_db
def test_user_create():
  User.objects.create_user('testuser', 'user@test.be', 'mypassword')
  assert User.objects.count() == 1

@pytest.mark.django_db
def test_events():
  assert Event.objects.count() > 3